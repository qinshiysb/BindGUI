# BindGUI #

Django 框架 Binder DNS图形化管理 使用Django Rest Framework API供客户端自动注册到服务器端数据库，并把内容实时的更新到Bind DNS中

项目参考了binder在原有的基础上进行了API自从注册的二次开发，方便客户端服务器能自动注册到DNS服务器中

之前也参考了一些其他互联网公司的方法，比如使用Bind-MySQL，可是配置不直观，也并不如原生文本那样稳定。同时增加了学习成本和部署难度，所以选择通过Bind DNS更新的原生方式，来投入生产使用

下载安装
git clone https://git.oschina.net/gibsonxue/BindGUI.git

需要Bind DNS 9.10以上的API支持，CentOS默认安装的是9.8.x或者9.9.x的已经是几年前的版本了，早不支持了，不知道为什么RedHat一直没有更新
可以尝试使用Ubuntu Server的Bind相对比较新

提供大家一个国外的个人Yum源，用这个源Yum出来的比较新
baseurl=http://bkraft.fr/files/RPM%20stuff/

## Python依赖文件 ##

The requirements.txt file has the necessary dependencies.

```
pip install -r requirements.txt
```

默认账号密码都是'admin'

目前项目为了调试方便，暂时使用的是SQLite，因为使用Django的ORM很方便的你就可以定义自己的MySQL或者其他数据库

* DJANGO_DB_HOST: IP address or Hostname of the MySQL database host. (Required)
* DJANGO_DB_NAME: Name of the MySQL database. (Required)
* DJANGO_DB_USER: Username to access the above database. (Optional. Default: binder)
* DJANGO_DB_PASSWORD: Binder Database password (Required)

输入一下几个命令就OK了
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```

项目开始之前你需要指定一个Binder TSIG，用于远程更新的Key

使用方法可以见如下网站
Starting with version 1.5, TSIG keys inside the database are encrypted using the [Crytography](https://cryptography.io/en/latest/fernet/) library and Fernet facilities.


### 配置 BIND DNS Server ###


#### named.conf ####
在named.conf 下面加入如下参数：开启Dns列表，定义监听statistics-channels

    options {
      zone-statistics yes;
    }

    statistics-channels {
        inet * port 8053 allow { 10.10.0.0/24; };
    };

举例，我使用的是 dynzone-key，这个在项目的settings里KEY_NAME="dynzone-key"可以自定义，不想修改我的代码就使用默认的


    zone "dynzone.yourdomain.org" IN {
        type master;
        file "/var/cache/bind/master/db.dynzone.yourdomain.org";
        allow-update { key dynzone-key; };
    };

#### /etc/bind/dynzone.key ####

Below are the entire contents of the dynzone.key file. This specifies the name, algorith and TSIG secret.

    key dynzone-key {
        algorithm hmac-md5;
        secret "";
    };

referenced as 'dynzone-key' in named.conf

更多内容英语好的同学可以看一下这个 TSIG see http://www.cyberciti.biz/faq/unix-linux-bind-named-configuring-tsig/ .

！！！另外要注意的是FERNET_KEY，可以通过以下命令获取，把值贴入settings中，非常关键，不然可能导致更新DNS的秘钥失效，秘钥是用MD5或者HASH形式保存在数据库的KEY表当中。
#FERNET_KEY=os.environ.get("DJANGO_FERNET_KEY", Fernet.generate_key())
FERNET_KEY='W7kCBzLcvQculFdM0Ecd39_kOsVAKteXRfeIbn1UuMQ='


### Apache配置文件 ###
config/binder-apache.conf.dist

### Nginx ###
config/binder-nginx.conf.dist

wsgi文件
config/django.wsgi

#### MySQL创建命令 ###

```
create database binder;

create user 'binder'@'%' identified by 'INSERTYOURPASSWORDHERE';

grant all privileges on binder.* to 'binder'@'%';

flush privileges;
```
