from django.conf.urls import include, url
from django.contrib import admin
import django.contrib.auth.views
import binder.views
from rest_framework import routers
admin.autodiscover()

binder_update = binder.views.BindRecordAPI.as_view({'get': 'retrieve','post': 'update'})

router = routers.DefaultRouter()
router.register(r'bindapi', binder.views.BindRecordAPI)


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    url(r'^update/(?P<pk>[a-zA-Z0-9.-]+)/$', binder_update, name='binder_update'),

    url(r'^accounts/login/$', django.contrib.auth.views.login, name='login'),
    url(r'^accounts/logout/$', django.contrib.auth.views.logout_then_login, name='logout'),

    url(r'^$', binder.views.home_index, name="index"),
    url(r'^server_list/$', binder.views.view_server_list, name="server_list"),

    url(r'^info/(?P<dns_server>[a-zA-Z0-9.-]+)/$', binder.views.view_server_zones, name="server_zone_list"),
    url(r'^info/(?P<dns_server>[a-zA-Z0-9.-]+)/(?P<zone_name>[a-zA-Z0-9.-]+)/$', binder.views.view_zone_records, name="zone_list"),

    url(r'^add_record/(?P<dns_server>[a-zA-Z0-9.-]+)/(?P<zone_name>[a-zA-Z0-9.-]+)/$', binder.views.view_add_record, name="add_record"),
    url(r'^add_cname/(?P<dns_server>[a-zA-Z0-9.-]+)/(?P<zone_name>[a-zA-Z0-9.-]+)/(?P<record_name>.*?)/$', binder.views.view_add_cname_record, name="add_cname"),
    url(r'^delete_record/(?P<dns_server>[a-zA-Z0-9.-]+)/(?P<zone_name>[a-zA-Z0-9.-]+)/$', binder.views.view_delete_record, name="delete_record"),
    url(r'^edit_record/(?P<dns_server>[a-zA-Z0-9.-]+)/(?P<zone_name>[a-zA-Z0-9.-]+)/(?P<record_name>[\S+]+)/(?P<record_data>[\S+]+)/(?P<record_ttl>[\S+]+)/$', binder.views.view_edit_record, name="edit_record"),
    url(r'^api/', include(router.urls))
]
