from rest_framework import routers, serializers, viewsets
from models import BindRecord

class BindSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BindRecord
        fields = ('dns_server','zone_name', 'record_name','record_data','ttl')